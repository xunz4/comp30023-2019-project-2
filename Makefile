CC = gcc
CFLAGS = -std=c99 -O3 -Wall -Wpedantic
EXE = crack

all: $(EXE)

sha256.o: sha256.c sha256.h
	$(CC) $(CFLAGS) -c sha256.c

$(EXE): crack.c sha256.o
	$(CC) $(CFLAGS) -o $(EXE) crack.c sha256.o

# test: test.c
# 	$(CC) $(CFLAGS) -o test test.c

clean:
	rm $(EXE) sha256.o
