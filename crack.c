// Header files
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sha256.h"

// constants
#define INPUTSIZE 10000
#define BUFFERSIZE 256

#define PWD4_FILENAME "pwd4sha256"
#define PWD6_FILENAME "pwd6sha256"
#define OUTPUT_FILENAME "found_pwds.txt"
#define DICT_FILENAME "passwords_dict.txt"
#define DICT_NUMBERS 10000
#define PWD4_DICT_FILENAME "common_pwd4.txt"
#define PWD6_DICT_FILENAME "common_pwd6.txt"
#define PWD4_NUMBERS 10
#define PWD_NUMBERS 30
#define PWD4_LEN 4
#define PWD6_LEN 6

#define ASCII_FROM 32
#define ASCII_TO 126
#define ASCII_NUM_FROM 48
#define ASCII_NUM_TO 57
#define ASCII_UPPERCASE_FROM 65
#define ASCII_UPPERCASE_TO 90
#define ASCII_LOWERCASE_FROM 97
#define ASCII_LOWERCASE_TO 122

// methods reference
int check_pwd4_guess(SHA256_CTX* ctx, BYTE hash[], char guess[],
                     BYTE pwd[][SHA256_BLOCK_SIZE], int checked[]);
int check_pwd6_guess(SHA256_CTX* ctx, BYTE hash[], char guess[],
                     BYTE pwd[][SHA256_BLOCK_SIZE], int checked[]);
int check_guess(SHA256_CTX* ctx, BYTE hash[], char guess[],
                BYTE pwd[][SHA256_BLOCK_SIZE], int pwdn, int checked[]);
void int_array_init(int array[], int n);

int main(int argc, char** argv) {
    if (argc == 1) {
        // store 30 passwords to guess
        FILE* fp;
        BYTE buffer[SHA256_BLOCK_SIZE];
        // create array to store hashes password
        BYTE pwd[PWD_NUMBERS][SHA256_BLOCK_SIZE];
        int status;

        fp = fopen(PWD4_FILENAME, "rb");

        int i;
        for (i = 0; i < PWD4_NUMBERS; i++) {
            // read 32 bytes to buffer
            status = fread(buffer, SHA256_BLOCK_SIZE, 1, fp);

            if (status < 0) {
                fprintf(stderr, "ERROR, reading binary file failed\n");
                exit(0);
            }

            memcpy(pwd[i], buffer, SHA256_BLOCK_SIZE);
        }
        fclose(fp);

        fp = fopen(PWD6_FILENAME, "rb");

        for (; i < PWD_NUMBERS; i++) {
            // read 32 bytes to buffer
            status = fread(buffer, SHA256_BLOCK_SIZE, 1, fp);

            if (status < 0) {
                fprintf(stderr, "ERROR, reading binary file failed\n");
                exit(0);
            }

            memcpy(pwd[i], buffer, SHA256_BLOCK_SIZE);
        }
        fclose(fp);

        // create guess and check it
        SHA256_CTX ctx;
        BYTE hash[SHA256_BLOCK_SIZE];
        int checked[PWD_NUMBERS];
        int_array_init(checked, PWD4_NUMBERS);

        // open output file
        fp = fopen(OUTPUT_FILENAME, "w");

        // violent crack PWD4
        char pw4guess[PWD4_LEN + 1];
        pw4guess[PWD4_LEN] = '\0';
        int find;

        int first, second, third, fourth;
        for (first = ASCII_FROM; first <= ASCII_TO; first++) {
            pw4guess[0] = first;
        for (second = ASCII_FROM; second <= ASCII_TO; second++) {
            pw4guess[1] = second;
        for (third = ASCII_FROM; third <= ASCII_TO; third++) {
            pw4guess[2] = third;
        for (fourth = ASCII_FROM; fourth <= ASCII_TO; fourth++) {
            pw4guess[3] = fourth;

            // generate hash and check
            find = check_pwd4_guess(&ctx, hash, pw4guess, pwd, checked);
            if (find){
                printf("%s %d\n", pw4guess, find);
                fprintf(fp, "%s %d\n", pw4guess, find);
            }
        }}}}

        // crack PWD6
        char pw6guess[PWD6_LEN + 1];
        pw6guess[PWD6_LEN] = '\0';
        int fifth, sixth;

        // guess pure number password
        for (first = ASCII_NUM_FROM; first <= ASCII_NUM_TO; first++) {
            pw6guess[0] = first;
        for (second = ASCII_NUM_FROM; second <= ASCII_NUM_TO; second++) {
            pw6guess[1] = second;
        for (third = ASCII_NUM_FROM; third <= ASCII_NUM_TO; third++) {
            pw6guess[2] = third;
        for (fourth = ASCII_NUM_FROM; fourth <= ASCII_NUM_TO;
                fourth++) {
            pw6guess[3] = fourth;
        for (fifth = ASCII_NUM_FROM; fifth <= ASCII_NUM_TO;
                fifth++) {
            pw6guess[4] = fifth;
        for (sixth = ASCII_NUM_FROM; sixth <= ASCII_NUM_TO;
                sixth++) {
            pw6guess[5] = sixth;

            // generate hash and check
            find = check_pwd6_guess(&ctx, hash, pw6guess, pwd, checked);
            if (find) {
                printf("%s %d\n", pw6guess, find);
                fprintf(fp, "%s %d\n", pw6guess, find);
            }
        }}}}}}

        // guess pure lowercase alphabets password, and first letter Capitalize
        for (first = ASCII_LOWERCASE_FROM; first <= ASCII_LOWERCASE_TO; first++) {
            pw6guess[0] = first;
        for (second = ASCII_LOWERCASE_FROM; second <= ASCII_LOWERCASE_TO; second++) {
            pw6guess[1] = second;
        for (third = ASCII_LOWERCASE_FROM; third <= ASCII_LOWERCASE_TO; third++) {
            pw6guess[2] = third;
        for (fourth = ASCII_LOWERCASE_FROM; fourth <= ASCII_LOWERCASE_TO;
                fourth++) {
            pw6guess[3] = fourth;
        for (fifth = ASCII_LOWERCASE_FROM; fifth <= ASCII_LOWERCASE_TO;
                fifth++) {
            pw6guess[4] = fifth;
        for (sixth = ASCII_LOWERCASE_FROM; sixth <= ASCII_LOWERCASE_TO;
                sixth++) {
            pw6guess[5] = sixth;

            // generate hash and check
            find = check_pwd6_guess(&ctx, hash, pw6guess, pwd, checked);
            if (find) {
                printf("%s %d\n", pw6guess, find);
                fprintf(fp, "%s %d\n", pw6guess, find);

                // continue find similar password
                // first lettle capitalize
                pw6guess[0] = first - ASCII_LOWERCASE_FROM + ASCII_UPPERCASE_FROM;

                find = check_pwd6_guess(&ctx, hash, pw6guess, pwd, checked);
                if (find) {
                    printf("%s %d\n", pw6guess, find);
                    fprintf(fp, "%s %d\n", pw6guess, find);
                }

                // init to lowercase for next guess
                pw6guess[0] = first;
            }
        }}}}}}

        // guess common pwd6
        FILE* dict_fp;
        dict_fp = fopen(PWD6_DICT_FILENAME, "r");
        char input[BUFFERSIZE];

        while(fgets(input, BUFFERSIZE, dict_fp) != NULL) {
            input[PWD6_LEN] = '\0';
            find = check_pwd6_guess(&ctx, hash, input, pwd, checked);
            if (find) {
                printf("%s %d\n", input, find);
                fprintf(fp, "%s %d\n", input, find);
            }
        }
        fclose(dict_fp);

        // guess common pwd4 plus 2 digits
        dict_fp = fopen(PWD4_DICT_FILENAME, "r");
        while (fgets(input, BUFFERSIZE, dict_fp) != NULL) {
            for (fifth = ASCII_NUM_FROM; fifth <= ASCII_NUM_TO; fifth++) {
                input[4] = fifth;
            for (sixth = ASCII_NUM_FROM; sixth <= ASCII_NUM_TO; sixth++) {
                input[5] = sixth;
                input[PWD6_LEN] = '\0';

                find = check_pwd6_guess(&ctx, hash, input, pwd, checked);
                if (find) {
                    printf("%s %d\n", input, find);
                    fprintf(fp, "%s %d\n", input, find);

                    // continue find similar password
                    char first_lettle, last_lettle;
                    first_lettle = input[0];
                    last_lettle = input[3];

                    // only first lettle capitalize
                    input[0] = first_lettle - ASCII_LOWERCASE_FROM +
                               ASCII_UPPERCASE_FROM;

                    find = check_pwd6_guess(&ctx, hash, input, pwd, checked);
                    if (find) {
                        printf("%s %d\n", input, find);
                        fprintf(fp, "%s %d\n", input, find);
                    }

                    // both first and last lettles capitalize
                    input[3] = last_lettle - ASCII_LOWERCASE_FROM +
                               ASCII_UPPERCASE_FROM;

                    find = check_pwd6_guess(&ctx, hash, input, pwd, checked);
                    if (find) {
                        printf("%s %d\n", input, find);
                        fprintf(fp, "%s %d\n", input, find);
                    }

                    // only last lettle capitalize
                    input[0] = first_lettle;

                    find = check_pwd6_guess(&ctx, hash, input, pwd, checked);
                    if (find) {
                        printf("%s %d\n", input, find);
                        fprintf(fp, "%s %d\n", input, find);
                    }

                    // init to lowercase for next guess
                    input[3] = last_lettle;
                }
            }}
        }
        fclose(dict_fp);

        fclose(fp);
    } else if (argc == 2) {
        // get the number of passwords to produce
        FILE* fp;
        char input[BUFFERSIZE];

        int n = atoi(argv[1]);

        fp = fopen(PWD6_DICT_FILENAME, "r");

        int i = 0;
        if (n <= DICT_NUMBERS) {
            while (++i <= n && fgets(input, BUFFERSIZE, fp) != NULL) {
                printf("%s", input);
            }
        } else {
            fprintf(stderr, "ERROR, too large n\n");
            exit(0);
        }

        fclose(fp);

    } else if (argc == 3) {
        FILE* fp;
        BYTE buffer[SHA256_BLOCK_SIZE];

        // read password file and store hashes passwords
        fp = fopen(argv[2], "rb");
        int status;

        // get the number of input passwords
        int pwdcount = 0, pwdsize = 0;

        fseek(fp, 0, SEEK_END);
        pwdsize = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        pwdcount = pwdsize / SHA256_BLOCK_SIZE;

        if (pwdsize % SHA256_BLOCK_SIZE) {
            fprintf(stderr, "ERROR, undefined password input file size\n");
            exit(0);
        }

        // create array to store hashes passwords
        BYTE pwd[pwdcount][SHA256_BLOCK_SIZE];

        int i;
        for (i = 0; i < pwdcount; i++) {
            // read 32 bytes to buffer
            status = fread(buffer, SHA256_BLOCK_SIZE, 1, fp);

            if (status < 0) {
                fprintf(stderr, "ERROR, reading binary file failed\n");
                exit(0);
            }

            memcpy(pwd[i], buffer, SHA256_BLOCK_SIZE);
        }
        fclose(fp);

        // read guess from file and check guess
        fp = fopen(argv[1], "r");
        char input[INPUTSIZE];
        SHA256_CTX ctx;
        BYTE hash[SHA256_BLOCK_SIZE];

        // create flag arrays to avoid repeat check
        int checked[pwdcount];
        int_array_init(checked, pwdcount);
        int find;

        while (fgets(input, INPUTSIZE, fp) != NULL) {
            // get rid of \n for buffer
            input[strlen(input) - 1] = '\0';

            // generate hash and check
            find = check_guess(&ctx, hash, input, pwd, pwdcount, checked);

            if (find) {
                printf("%s %d\n", input, find);
            }
        }

        fclose(fp);
    } else {
        fprintf(stderr, "ERROR, undefined usage of crack\n");
        exit(0);
    }

    return 0;
}

int check_pwd4_guess(SHA256_CTX* ctx, BYTE hash[], char guess[],
                     BYTE pwd[][SHA256_BLOCK_SIZE], int checked[]) {
    sha256_init(ctx);
    sha256_update(ctx, (BYTE*)guess, PWD4_LEN);
    sha256_final(ctx, hash);

    int i;
    for (i = 0; i < PWD4_NUMBERS; i++) {
        if (!checked[i] && !memcmp(pwd[i], hash, SHA256_BLOCK_SIZE)) {
            checked[i] = 1;
            return i + 1;
        }
    }

    return 0;
}
int check_pwd6_guess(SHA256_CTX* ctx, BYTE hash[], char guess[],
                     BYTE pwd[][SHA256_BLOCK_SIZE], int checked[]) {
    sha256_init(ctx);
    sha256_update(ctx, (BYTE*)guess, PWD6_LEN);
    sha256_final(ctx, hash);

    int i;
    for (i = PWD4_NUMBERS; i < PWD_NUMBERS; i++) {
        if (!checked[i] && !memcmp(pwd[i], hash, SHA256_BLOCK_SIZE)) {
            checked[i] = 1;
            return i + 1;
        }
    }

    return 0;
}

int check_guess(SHA256_CTX* ctx, BYTE hash[], char guess[],
                BYTE pwd[][SHA256_BLOCK_SIZE], int pwdn, int checked[]) {
    sha256_init(ctx);
    sha256_update(ctx, (BYTE*)guess, PWD6_LEN);
    sha256_final(ctx, hash);

    int i;
    for (i = 0; i < pwdn; i++) {
        if (!checked[i] && !memcmp(pwd[i], hash, SHA256_BLOCK_SIZE)) {
            checked[i] = 1;
            return i + 1;
        }
    }

    return 0;
}

void int_array_init(int array[], int n) {
    for (int i = 0; i < n; i++) array[i] = 0;
}