// Header files
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// constants
#define HOSTNAME "172.26.37.44"
#define PORT 7800
#define COMMAND "openssl sha256 dh.c"
#define RESULT_TITLE "SHA256(dh.c)= "
#define RESULT_TITLE_LEN 14
#define BUFFERSIZE 256
#define MESSAGE_ENDING '\n'
#define USERNAME "xunz4"
#define g_Value 15
#define p_Value 97

// methods reference
int get_b_Value(char* hashresult);
int calculate(int base, int pow, int modulus);

int main(int argc, char** argv) {
    // used to save message
    char buffer[BUFFERSIZE];
    // used to save status
    int status;
    int b_Value;

    // get b value
    FILE* fp;
    fp = popen(COMMAND, "r");
    memset(buffer, '\0', BUFFERSIZE);
    if (fgets(buffer, sizeof(buffer), fp) != NULL && strstr(buffer, RESULT_TITLE) != NULL) {
        char* hashresult = strstr(buffer, RESULT_TITLE) + RESULT_TITLE_LEN;
        b_Value = get_b_Value(hashresult);
    }

    // connect to server
    int sockfd;
    struct sockaddr_in serv_addr;
    struct hostent* server;

    // build server
    server = gethostbyname(HOSTNAME);

    if (server == NULL) {
        fprintf(stderr, "ERROR, no such host\n");
        exit(0);
    }

    // Building data structures for socket

    memset((char*)&serv_addr, '\0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;

    bcopy(server->h_addr_list[0], (char*)&serv_addr.sin_addr.s_addr,
          server->h_length);

    serv_addr.sin_port = htons(PORT);

    /* Create TCP socket -- active open
     * Preliminary steps: Setup: creation of active open socket
     */

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0) {
        perror("ERROR opening socket");
        exit(0);
    }

    if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("ERROR connecting");
        exit(0);
    }

    // Do processing
    // send username
    memset(buffer, '\0', BUFFERSIZE);

    strcpy(buffer, USERNAME);
    buffer[strlen(buffer)] = MESSAGE_ENDING;

    status = write(sockfd, buffer, strlen(buffer));

    if (status < 0) {
        perror("ERROR writing to socket");
        exit(0);
    }

    // send g^b (mod p)
    memset(buffer, '\0', BUFFERSIZE);

    int B = calculate(g_Value, b_Value, p_Value);
    sprintf(buffer, "%d", B);
    buffer[strlen(buffer)] = MESSAGE_ENDING;

    status = write(sockfd, buffer, strlen(buffer));

    if (status < 0) {
        perror("ERROR writing to socket");
        exit(0);
    }

    // receive g^a (mod p)
    memset(buffer, '\0', BUFFERSIZE);

    status = read(sockfd, buffer, BUFFERSIZE - 1);

    if (status < 0) {
        perror("ERROR reading from socket");
        exit(0);
    }

    int A = atoi(buffer);

    // send key A^b (mod p)
    memset(buffer, '\0', BUFFERSIZE);

    int keyB = calculate(A, b_Value, p_Value);
    sprintf(buffer, "%d", keyB);
    buffer[strlen(buffer)] = MESSAGE_ENDING;

    status = write(sockfd, buffer, strlen(buffer));

    if (status < 0) {
        perror("ERROR writing to socket");
        exit(0);
    }

    // receive status report
    memset(buffer, '\0', BUFFERSIZE);

    status = read(sockfd, buffer, BUFFERSIZE - 1);

    if (status < 0) {
        perror("ERROR reading from socket");
        exit(0);
    }

    printf("%s\n", buffer);

    return 0;
}

/* get the hashresult string and convert the rst two hexadecimal digits to an
 * integer
 */
int get_b_Value(char* hashresult) {
    char substring[3];
    memset(substring, '\0', sizeof(substring));
    strncpy(substring, hashresult, 2);

    int hex;
    sscanf(substring, "%x", &hex);

    return hex;
}

/* Since the limit of int, we have to use supplement function which applying
 * Modular Exponentiation algorithm for the calculation of g^b (modp)
 */
int calculate(int base, int pow, int modulus) {
    int odd;
    int output = 1;

    while (pow > 0) {
        odd = pow % 2;

        if (odd) output = (output * base) % modulus;
        base = base * base % modulus;

        pow = pow / 2;
    }

    return output;
}
